# Репозиторий с jupyter notebooks по machine learning

* `HSE_ML_course`: лабораторные работы с курса по машинному обучению (2020)
* `HSE_neural_networks_course`: лабораторные работы с курса по нейронным сетям (2021)
* `YA_PROFI`: решения олимпиады "Я-профессионал" (2021)
